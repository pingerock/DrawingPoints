﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingPoints
{
    // Task 1
    public class Point : Figure, IPaintable
    {
        public readonly string name;
        public readonly double x;
        public readonly double y;
        public readonly double z;
        public readonly int dimension;

        public Point(string name, double x)
        {
            this.name = name;
            this.x = x;
            dimension = 1;
        }
        public Point(string name, double x, double y)
        {
            this.name = name;
            this.x = x;
            this.y = y;
            dimension = 2;
        }

        public Point(string name, double x, double y, double z)
        {
            this.name = name;
            this.x = x;
            this.y = y;
            this.z = z;
            dimension = 3;
        }

        // Task 3
        /*public static string GetInfo(Point point)
        {
            return $"Координата точки {point.name}: {point.x}";
        }*/

        // Task 4
        /*public static string GetInfo(Point point, bool showName)
        {
            if (showName)
            {
                return $"Координата точки {point.name}: {point.x}";
            }
            else
            {
                return $"Координата точки: {point.x}";
            }
        }*/

        public static string GetNameWithCoordinates(Point point)
        {
            return $"Точка {point.name}: количество координат: {point.dimension}";
        }

        public void Paint()
        {
            switch (dimension)
            {
                case 1:
                    Console.WriteLine($"Координаты точки {name}: {x}");
                    break;
                case 2:
                    Console.WriteLine($"Координаты точки {name}: {x}, {y}");
                    break;
                case 3:
                    Console.WriteLine($"Координаты точки {name}: {x}, {y}, {z}");
                    break;
                default:
                    Console.WriteLine("Ошибка! Нет данных о координатах.");
                    break;
            }
        }
    }
}
