﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingPoints
{
    // Task 6
    public class Figure
    {
        // Сделал методы виртуальными, чтобы можно было, в случае чего, реализовать через сам класс Figure.
        public virtual int GetDimension()
        {
            //В классе Figure нет полей, чтобы что-то выводить. Однако в классе Point он будет переопределён.
            return 0;
        }

        public virtual string GetFigureType()
        {
            //Не думаю, что одна точка будет хранить данные о типе фигуры. Этот метод лучше оставить только в классе
            //Figure, но в задаче указано, чтобы этот метод тоже наследовался. Поэтому оставлю его виртуальным, чтобы
            //в нем был функционал в базовом классе.
            return "Фигура";
        }
    }
}
